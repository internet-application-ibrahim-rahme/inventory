package com.example.cars;

import com.example.cars.configs.RabbitMq;
import com.example.cars.dto.RequestReport;
import com.example.cars.entities.Car;
import com.example.cars.repositories.CarRepository;
import org.junit.jupiter.api.Test;
import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import static org.mockito.Mockito.when;

@SpringBootTest
//@AutoConfigureMockMvc
class CarsApplicationTests {

	@Autowired
	private RabbitTemplate rabbitTemplate;

//    @MockBean
//    private CarRepository carRepository;
	@Test
	void contextLoads() {
	}
	@Test
	void testQueue(){
//        List<Car> carsMock = new ArrayList<Car>();
//        carsMock.add(new Car(Long.valueOf(1),"marceds",120,4, new Date(),400, "Ibrahim uncle"));
//        carsMock.add(new Car(Long.valueOf(2), "BMW",120,4, new Date(),400, "Ibrahim uncle"));
//        carsMock.add(new Car(Long.valueOf(3), "AUDI",120,4, new Date(),400, "Ibrahim uncle"));
//	    Integer mon = new Date().getMonth()+1;
//        when(carRepository.findCarsBySaleDate(mon)).thenReturn(carsMock);
		RequestReport temp = new RequestReport("hellbeso@gmail.com","testing",new Date());
		rabbitTemplate.convertAndSend(RabbitMq.topicExchangeName,"foo.bar.baz", temp);
	}

}
