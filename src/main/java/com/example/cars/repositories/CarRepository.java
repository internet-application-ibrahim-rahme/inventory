package com.example.cars.repositories;

import com.example.cars.entities.Car;
import org.springframework.data.jpa.repository.Lock;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.LockModeType;
import java.util.Date;
import java.util.List;
import java.util.Optional;

@Repository
public interface CarRepository extends CrudRepository<Car,Long> {

    List<Car> findBySaleDate(Date sale_date);
    List<Car> findByNameLike(String nameLike);


    @Transactional
    @Lock(LockModeType.PESSIMISTIC_READ)
    @Query(value = "SELECT c FROM Car c WHERE c.saleDate is not null  and FUNCTION('month',c.saleDate )  = :saleDate")
    List<Car> findCarsBySaleDate(@Param("saleDate") Integer saleDate);

    List<Car> findCarsBySaleDateIsNotNullAndSaleDate(Integer month);
//    List<Car> findAllBySaleDate_Month(Integer month);
}
