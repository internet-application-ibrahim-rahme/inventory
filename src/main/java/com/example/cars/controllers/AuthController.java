package com.example.cars.controllers;

import com.example.cars.dto.LoginDto;
import com.example.cars.dto.UserRegister;
import com.example.cars.entities.User;
import com.example.cars.services.AuthService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController// This means that this class is a Controller
@RequestMapping("/auth") // This means URL's start with /auth (after Application path)
public class AuthController {

    private AuthService authService;

    @Autowired
    AuthController(AuthService authService) {
        this.authService = authService;
    }

    @PostMapping("/register")
    public ResponseEntity<User> register(@RequestBody UserRegister userDto) {
        System.out.println("register");
        return this.authService.register(userDto);

    }

//    @PostMapping("/login")
//    public ResponseEntity<User> login(@RequestBody LoginDto loginDto) {
//        return this.authService.login(loginDto);
//    }
}
