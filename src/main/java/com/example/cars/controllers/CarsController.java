package com.example.cars.controllers;

import com.example.cars.dto.CarDto;
import com.example.cars.dto.SellCarDto;
import com.example.cars.entities.Car;
import com.example.cars.services.CarService;
import org.springframework.beans.factory.annotation.Required;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/api/car")
public class CarsController {

    private CarService carService;

    CarsController(CarService carService) {
        this.carService = carService;
    }

    @GetMapping("/")
    public ResponseEntity<List<Car>> index(){
        return carService.index();
    }

    @GetMapping("/search")
    public ResponseEntity<List<Car>> search(@RequestParam(name = "q") String q){
        return carService.search(q);
    }

    @PostMapping("/")
    public ResponseEntity<Car> store(@RequestBody CarDto carDto){
        return carService.store(carDto);
    }

    @PatchMapping("/{id}")
    public ResponseEntity<Car> update(@PathVariable Long id, @RequestBody CarDto carDto){
        return carService.update(id, carDto);
    }

    @DeleteMapping("/{id}")
    public ResponseEntity<Car> delete(@PathVariable Long id){
        return carService.delete(id);
    }

    @PostMapping("/{id}")
    public ResponseEntity<Car> sell(@PathVariable Long id, @RequestBody SellCarDto carDto){
        return carService.sell(id,carDto);
    }
}
