package com.example.cars.entities;

import com.sun.istack.NotNull;
import org.hibernate.annotations.CreationTimestamp;
import org.hibernate.annotations.Generated;
import org.hibernate.annotations.UpdateTimestamp;

import javax.persistence.*;
import java.util.Date;

@Entity
public class Car {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    @Column(nullable = false)
    private String name;
    @Column(nullable = false)
    private Integer price;
    @Column(nullable = false)
    private Integer seatCount;
    @Temporal(TemporalType.DATE)
    private Date saleDate;
    private Integer salePrice;
    private String buyerName;

    @CreationTimestamp
    private Date created_at;

    @UpdateTimestamp
    private Date updated_at;

    public Car(){}
    public Car(String name, Integer price, Integer seatCount) {
        this.name = name;
        this.price = price;
        this.seatCount = seatCount;
        this.buyerName = buyerName;
    }

    public Car(Long id , String name, Integer price, Integer seatCount, Date saleDate, Integer salePrice, String buyerName) {
        this.id = id;
        this.name = name;
        this.price = price;
        this.seatCount = seatCount;
        this.saleDate = saleDate;
        this.salePrice = salePrice;
        this.buyerName = buyerName;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Integer getPrice() {
        return price;
    }

    public void setPrice(Integer price) {
        this.price = price;
    }

    public Integer getSeatCount() {
        return seatCount;
    }

    public void setSeatCount(Integer seatCount) {
        this.seatCount = seatCount;
    }

    public Date getSaleDate() {
        return saleDate;
    }

    public void setSaleDate(Date saleDate) {
        this.saleDate = saleDate;
    }

    public Integer getSalePrice() {
        return salePrice;
    }

    public void setSalePrice(Integer salePrice) {
        this.salePrice = salePrice;
    }

    public String getBuyerName() {
        return buyerName;
    }

    public void setBuyerName(String buyerName) {
        this.buyerName = buyerName;
    }

    public String[] toStringArray() {
        String[] results = new String[7];
        results[0] = String.valueOf(id);
        results[1] = name;
        results[2] = String.valueOf(price);
        results[3] = String.valueOf(seatCount);
        results[4] = String.valueOf(saleDate);
        results[5] = String.valueOf(salePrice);
        results[6] = String.valueOf(buyerName);
        return results;
    }


    public String[] toStringArray(Integer withSum) {
        String[] results = new String[8];
        results[0] = String.valueOf(id);
        results[1] = name;
        results[2] = String.valueOf(price);
        results[3] = String.valueOf(seatCount);
        results[4] = String.valueOf(saleDate);
        results[5] = String.valueOf(salePrice);
        results[6] = String.valueOf(buyerName);
        results[6] = String.valueOf(withSum);
        return results;
    }
}
