package com.example.cars.listeners;

import com.example.cars.dto.RequestReport;
import com.example.cars.entities.Car;
import com.example.cars.repositories.CarRepository;
import com.example.cars.services.CarService;
import com.example.cars.services.EmailService;
import org.apache.commons.csv.CSVFormat;
import org.apache.commons.csv.CSVPrinter;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Service;

import javax.mail.MessagingException;
import javax.persistence.EntityManager;
import javax.persistence.LockModeType;
import javax.persistence.Query;
import javax.transaction.Transactional;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

@Service
public class RabbitMqReceiver {

    @Autowired
    private EmailService emailService;
    @Autowired
    private CarRepository carRepository;
    @Autowired
    private CarService carService;
    // entity manager because you cant autowired service and repositories from here
    private EntityManager entityManager;

    private RabbitMqReceiver(EntityManager entityManager) {
        this.entityManager = entityManager;
    }

    public void receiveMessage(RequestReport message) {
        System.out.println("message = [" + message.date + "]");


        // 1- get the cars the sold in that month (Todo: and year)
        // Integer month = message.date.getMonth() + 1;
        // Query query = entityManager.createNativeQuery("SELECT id,name,buyer_name,price,sale_date,sale_price,seat_count FROM cars.Car as c WHERE c.sale_date is not null  and month(c.sale_date) = " + month + "");
        // List<Object[]> cars = (List<Object[]>) query.getResultList();
        Integer mon = message.date.getMonth() + 1;
        System.out.println(mon);
        List<Car> carsO = carRepository.findCarsBySaleDate(mon);
        System.out.println(carsO.size());

        Integer sumOfPrices = 0;
        for (int i = 0; i < carsO.size(); i++) {
            sumOfPrices += carsO.get(i).getPrice();
        }
        // 2 - convert each record to array of string to handle them in csv format
        List<String[]> outputs = new ArrayList<>();
        carsO.forEach(car -> {
            outputs.add(car.toStringArray());
        });
        /*for (int i = 0; i < cars.size(); i++) {
            String[] temp = new String[7];
            for (int j = 0; j < cars.get(i).length; j++) {
                temp[j] = cars.get(i)[j] + "";
            }
            outputs.add(temp);
        }
*/
        // 3- create csv file
        File report = null;
        try {
            report = convertListToCsvFile(outputs);
            // 4- call the Mail service
            emailService.sendMessageWithAttachment(message.email, message.content, "Total sum: " + sumOfPrices, report);

        } catch (MessagingException | IOException e) {
            e.printStackTrace();
        }

    }

    public File convertListToCsvFile(List<String[]> dataLines) throws IOException {
        File csvOutputFile = new File("out.csv");
        try (PrintWriter pw = new PrintWriter(csvOutputFile)) {
            dataLines.stream()
                    .map(this::convertToCSV)
                    .forEach(pw::println);
        }
        return csvOutputFile;
    }

    public String convertToCSV(String[] data) {
        return Stream.of(data)
                .map(this::escapeSpecialCharacters)
                .collect(Collectors.joining(","));
    }

    public String escapeSpecialCharacters(String data) {
        String escapedData = data.replaceAll("\\R", " ");
        if (data.contains(",") || data.contains("\"") || data.contains("'")) {
            data = data.replace("\"", "\"\"");
            escapedData = "\"" + data + "\"";
        }
        return escapedData;
    }

}
