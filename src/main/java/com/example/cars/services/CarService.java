package com.example.cars.services;

import com.example.cars.dto.CarDto;
import com.example.cars.dto.SellCarDto;
import com.example.cars.entities.Car;
import com.example.cars.repositories.CarRepository;
import com.example.cars.repositories.ParameterRepo;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.lang.invoke.ConstantBootstraps;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.NoSuchElementException;

@Service
public class CarService {

    private CarRepository carRepository;
    private ParameterRepo parameterRepo;

    private CarService(CarRepository carRepository, ParameterRepo parameterRepo) {
        this.carRepository = carRepository;
        this.parameterRepo = parameterRepo;
    }

    public CarService(){}
    /**
     * Get all not sold Cars
     * @return
     */
    public ResponseEntity<List<Car>> index() {

        return ResponseEntity.ok(carRepository.findBySaleDate(null));
    }

    /**
     * Store a new Care
     *
     * @param carDto
     * @return
     */
    public ResponseEntity<Car> store(CarDto carDto) {
        // 1- the the seat number if not present

        if (carDto.seatCount == null) {
            // get from the parameter
            carDto.seatCount = Integer.parseInt(parameterRepo.findBypKey("SEAT_COUNT").get().getpValue());
        }
        Car car = new Car(carDto.name, carDto.price, carDto.seatCount);

        return ResponseEntity.ok(carRepository.save(car));
    }

    /**
     * Update an old car
     *
     * @param id
     * @param carDto
     * @return
     */
    public ResponseEntity<Car> update(Long id, CarDto carDto) {

        // 1- get the car
        Car car = carRepository.findById(id).get();

        // 2- update the car
        car.setName((carDto.name != null) ? carDto.name : car.getName());
        car.setPrice((carDto.price != null) ? carDto.price : car.getPrice());
        car.setSeatCount((carDto.seatCount != null) ? carDto.seatCount : car.getSeatCount());

        return ResponseEntity.ok(carRepository.save(car));
    }

    /**
     * Delete Car
     *
     * @param id
     * @return
     */
    public ResponseEntity<Car> delete(Long id) {
        // 1- get the car
        Car car = carRepository.findById(id).get();
        carRepository.deleteById(id);
        return ResponseEntity.ok(car);
    }

    /**
     * Sell old car
     *
     * @param carDto
     * @return
     */
    public ResponseEntity<Car> sell(Long id, SellCarDto carDto) {
        // 1- get the car
        Car car = new Car();
        try {
            car = carRepository.findById(id).get();

        } catch (NoSuchElementException e) {
            return new ResponseEntity<Car>(car, HttpStatus.NOT_FOUND);
        }

        // 2- get the PROFIT_PERCENTAGE parameter
        if (carDto.sellPrice == null) {
            Integer temp = Integer.parseInt(parameterRepo.findBypKey("PROFIT_PERCENTAGE").get().getpValue());
            carDto.sellPrice = car.getPrice() + car.getPrice() * temp / 100;// original price 200 + 10% profit = 220
        }

        // 3- add the sell information
        car.setSalePrice(carDto.sellPrice);
        car.setBuyerName(carDto.buyerName);
        car.setSaleDate(new Date());

        return ResponseEntity.ok(carRepository.save(car));
    }


    public ResponseEntity<List<Car>> search(String nameLike){
        return ResponseEntity.ok(carRepository.findByNameLike(nameLike));
    }


//    @Transactional
//    public List<Car> getAllCarsSalesByMonth(Date date){
//        Integer mon = date.getMonth() + 1;
//        List<Car> carsO = carRepository.findCarsBySaleDate(mon);
//        return carsO;
//    }
}
