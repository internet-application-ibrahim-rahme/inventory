package com.example.cars.dto;

import java.io.Serializable;
import java.util.Date;

public class RequestReport implements Serializable {
    public String email;
    public String content;
    public Date date;

    public RequestReport(String email, String content, Date date) {
        this.email = email;
        this.content = content;
        this.date = date;
    }
}
