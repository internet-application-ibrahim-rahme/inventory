package com.example.cars.dto;

public class SellCarDto {

    public Integer sellPrice;
    public String buyerName;

    public SellCarDto(Integer sellPrice, String buyerName) {
        this.sellPrice = sellPrice;
        this.buyerName = buyerName;
    }
}
